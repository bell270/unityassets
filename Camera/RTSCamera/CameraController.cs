using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float moveSpeed = 20f;
    public float panBoardThickness = 10f;
    public Vector2 panLimit;
    public Vector2 zoomLimit;
    public float scrollSpeed = 2000f;

    // Start is called before the first frame update
    void Start()
    {
        
    }


    void Update()
    {
        Vector3 cameraPosition = transform.position;
        float orthoSize = GetComponent<Camera>().orthographicSize;

        if (Input.GetKey(KeyCode.W) 
            //|| Input.mousePosition.y > Screen.height - panBoardThickness
            )
        {
            cameraPosition.y += moveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S) 
            //|| Input.mousePosition.y < panBoardThickness
            )
        {
            cameraPosition.y -= moveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D)
            //|| Input.mousePosition.x > Screen.width - panBoardThickness
            )
        {
            cameraPosition.x += moveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A)
            //|| Input.mousePosition.x < panBoardThickness
            )
        {
            cameraPosition.x -= moveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.E))
        {
            orthoSize -= moveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.Q))
        {
            orthoSize += moveSpeed * Time.deltaTime;
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        orthoSize -= scroll * scrollSpeed * Time.deltaTime;

        cameraPosition.x = Mathf.Clamp(cameraPosition.x, -panLimit.x, panLimit.x);
        cameraPosition.y = Mathf.Clamp(cameraPosition.y, -panLimit.y, panLimit.y);
        orthoSize = Mathf.Clamp(orthoSize, zoomLimit.x, zoomLimit.y);
        GetComponent<Camera>().orthographicSize = orthoSize;
        transform.position = cameraPosition;
    }
}
