using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public bool isPaused = false;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPaused)
        {
            if (Input.GetMouseButtonDown(0))
            {
                StartCoroutine(FindObjectOfType<CameraShake>().Shake(.15f, .4f));
            }
            if (Input.GetMouseButtonDown(1)) // DEAD
            {
                Time.timeScale = 0;
                isPaused = true;
                SceneManager.LoadScene(SceneManager.sceneCountInBuildSettings - 1, LoadSceneMode.Additive);
            }

        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(isPaused)
            {
                isPaused = false;
                Time.timeScale = 1;
                _ = SceneManager.UnloadSceneAsync(1);
            }
            else
            {
                isPaused = true;
                Time.timeScale = 0;
                SceneManager.LoadScene(1, LoadSceneMode.Additive);
            }

        }
    }

    private void FixedUpdate()
    {
    }
}
