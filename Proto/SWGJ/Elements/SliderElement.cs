using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderElement : MonoBehaviour
{
    public SliderHandle handle;
    public LineRenderer limit;
    public bool isLimitVisible = true;

    // Start is called before the first frame update
    void Start()
    {
        limit = GetComponent<LineRenderer>();
        limit.enabled = isLimitVisible;
        handle.SetParentSlider(this);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
