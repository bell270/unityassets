using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderHandle : MonoBehaviour
{
    private SliderElement slider;
    private SpriteRenderer handleRenderer;
    private Color startcolor;
    private bool isHold = false;
    private Rigidbody2D _rb;
    // Start is called before the first frame update
    void Start()
    {
        handleRenderer = GetComponent<SpriteRenderer>();
        _rb = GetComponent<Rigidbody2D>();
        _rb.gravityScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnMouseEnter()
    {
        if (!isHold)
        {
            startcolor = handleRenderer.color;
            handleRenderer.color = Color.cyan;
        }
    }

    void OnMouseExit()
    {
        if (!isHold)
        {
            handleRenderer.color = startcolor;
        }
    }

    private void OnMouseDown()
    {
        handleRenderer.color = Color.blue;
        isHold = true;
    }

    private void OnMouseUp()
    {
        handleRenderer.color = startcolor;
        isHold = false;
    }

    private void OnMouseDrag()
    {
        Vector3 mousePos= Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MoveHandle(new Vector2(mousePos.x, mousePos.y));
    }

    private void MoveHandle(Vector2 mousePos)
    {
        Vector2 closestPoint = _rb.position;
        for (int i = 0; i < slider.limit.positionCount - 1; i++)
        {
            Vector2 origin = slider.transform.TransformPoint(slider.limit.GetPosition(i));
            Vector2 end = slider.transform.TransformPoint(slider.limit.GetPosition(i + 1));
            //Get heading
            Vector2 heading = (end - origin);
            float magnitudeMax = heading.magnitude;
            heading.Normalize();

            //Do projection from the point but clamp it
            Vector2 lhs = mousePos - origin;
            float dotP = Vector2.Dot(lhs, heading);
            dotP = Mathf.Clamp(dotP, 0f, magnitudeMax);
            Vector2 result = origin + heading * dotP;
            if (Vector2.Distance(result, mousePos) < Vector2.Distance(closestPoint, mousePos))
            {
                closestPoint = result;
            }
        }
        _rb.MovePosition(closestPoint);
    }

    public void SetParentSlider(SliderElement slider)
    {
        this.slider = slider;
    }
}
