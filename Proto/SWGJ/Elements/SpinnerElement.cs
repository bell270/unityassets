using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerElement : MonoBehaviour
{
    public bool isTurner = false;
    public Transform pivotPoint;
    public List<Transform> limits;
    public float rotationSpeed = 10f;

    public SpinnerHandle handle;

    void Start()
    {
        handle.SetParentSpinner(this);
        handle.SetType(isTurner);
    }

    public Vector2 GetBorderAngles()
    {
        float x = Vector2.SignedAngle(Vector2.right, limits[0].position);
        float y = Vector2.SignedAngle(Vector2.right, limits[1].position);
        if (x > y)
        {
            float t = x;
            x = y;
            y = t;
        }
        return new Vector2(x, y);
    }
}
