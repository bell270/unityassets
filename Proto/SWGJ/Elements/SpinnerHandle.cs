using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerHandle : MonoBehaviour
{
    private bool isTurner;
    private SpinnerElement spinner;
    private SpriteRenderer handleRenderer;
    private Color startcolor;

    private bool isHold = false;
    private Vector2 mouseDownPos;
    private float mouseDownAngle;

    void Start()
    {
        handleRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnMouseEnter()
    {
        if (!isHold)
        {
            startcolor = handleRenderer.color;
            handleRenderer.color = Color.cyan;
        }
    }

    void OnMouseExit()
    {
        if (!isHold)
        {
            handleRenderer.color = startcolor;
        }
    }

    private void OnMouseDown()
    {
        handleRenderer.color = Color.blue;
        isHold = true;
        mouseDownPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouseDownAngle = Vector2.SignedAngle(Vector2.right, transform.right);
    }

    private void OnMouseUp()
    {
        handleRenderer.color = startcolor;
        isHold = false;
    }

    private void OnMouseDrag()
    {
        Vector3 mousePos= Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MoveHandle(new Vector2(mousePos.x, mousePos.y));
    }
    //public void LookAt(Vector2 lookAt)
    //{
    //    Vector2 direction = lookAt - new Vector2(transform.position.x, transform.position.y);
    //    float angle = Vector2.SignedAngle(transform.right, direction);
    //    float angleDiff = angle - _rb.rotation;
    //    float newAngle = (_rb.rotation + rotationSpeed * angle * Time.fixedDeltaTime) % 360;
    //    _rb.rotation = newAngle;
    //}

    private void MoveHandle(Vector2 mousePos)
    {
        Vector2 startMouseVector = new Vector2(mouseDownPos.x - spinner.pivotPoint.position.x, mouseDownPos.y - spinner.pivotPoint.position.y);
        Vector2 curMouseVector = new Vector2(mousePos.x - spinner.pivotPoint.position.x, mousePos.y - spinner.pivotPoint.position.y);
        float mouseAngle = Vector2.SignedAngle(startMouseVector, curMouseVector);
        float newAngle = mouseDownAngle + mouseAngle;
        float curAngle = Vector2.SignedAngle(Vector2.right, transform.right);
        float angleDiff = newAngle - curAngle;
        float newRotation = (transform.rotation.eulerAngles.z + newAngle - curAngle) % 360;
        if (isTurner)
        {
            Vector2 borderAngles = spinner.GetBorderAngles();
            newRotation = Mathf.Clamp(newRotation, borderAngles.x, borderAngles.y);
            Debug.Log(borderAngles + " " + newRotation);
        }
        Quaternion curRot = transform.rotation;
        curRot.eulerAngles = new Vector3(0, 0, newRotation);
        transform.rotation = curRot;
    }

    public void SetParentSpinner(SpinnerElement spinner)
    {
        this.spinner = spinner;
    }

    public void SetType(bool isTurner)
    {
        this.isTurner = isTurner;
    }
}
