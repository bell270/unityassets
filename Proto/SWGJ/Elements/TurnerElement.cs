using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnerElement : MonoBehaviour
{
    public Transform pivotPoint;
    public List<Transform> limit;

    public TurnerHandle handle;

    void Start()
    {
        handle.SetParentTurner(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
