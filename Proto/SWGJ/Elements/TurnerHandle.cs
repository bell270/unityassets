using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnerHandle : MonoBehaviour
{
    private TurnerElement turner;
    private SpriteRenderer handleRenderer;
    private Color startcolor;
    private bool isHold = false;
    // Start is called before the first frame update
    void Start()
    {
        handleRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnMouseEnter()
    {
        if (!isHold)
        {
            startcolor = handleRenderer.color;
            handleRenderer.color = Color.cyan;
        }
    }

    void OnMouseExit()
    {
        if (!isHold)
        {
            handleRenderer.color = startcolor;
        }
    }

    private void OnMouseDown()
    {
        handleRenderer.color = Color.blue;
        isHold = true;
    }

    private void OnMouseUp()
    {
        handleRenderer.color = startcolor;
        isHold = false;
    }

    private void OnMouseDrag()
    {
        Vector3 mousePos= Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MoveHandle(new Vector2(mousePos.x, mousePos.y));
    }

    private void MoveHandle(Vector2 mousePos)
    {
    }

    public void SetParentTurner(TurnerElement turner)
    {
        this.turner = turner;
    }
}
